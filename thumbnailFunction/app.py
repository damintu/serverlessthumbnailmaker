import boto3
import os
import sys
import uuid
from urllib.parse import unquote_plus
from PIL import Image
import PIL.Image
from PIL.ExifTags import TAGS
s3_client = boto3.client('s3')

def resize_image(image_path: str, resized_path: str, size: tuple):
  with Image.open(image_path) as image:
      image.thumbnail(size)
      image.save(resized_path)

def extract_exif (image_path: str,exif_path: str):
  with Image.open(image_path) as image:
    exifdata = image.getexif()
    f = open(exif_path, "w")
    f.write(str(exifdata))
    f.close()

def lambda_handler(event, context):
  for record in event['Records']:
      ##Variables

      #Get the prefix of the generated image and the meta to be uploaded in the s3 bucket, removing the trailing '/' if present.
      thumbnail_prefix = os.getenv('THUMBNAIL_PREFIX', 'thumbnail')
      thumbnail_prefix = thumbnail_prefix.rstrip('/')
      meta_prefix = os.getenv('META_PREFIX', 'meta')
      meta_prefix = meta_prefix.rstrip('/')

      #Retreave the height and width to resize the original image to
      #Default value to 50px
      height = (os.getenv('HEIGHT', 50))
      width = (os.getenv('WIDTH', 50))
      size = (height,width)

      bucket = record['s3']['bucket']['name']
      key = unquote_plus(record['s3']['object']['key'])

      #TODO Make a folder with the uuid and store info there.
      #Strips the prefix part of the object key
      striped_key = key.split('/')[-1]

      #Working dirs & paths
      base_path = '/tmp/{}/'.format(uuid.uuid4())
      download_path = base_path + striped_key
      thumbnail_path = base_path + 'resized-{}'.format(striped_key)
      exif_path = base_path + 'meta-{}'.format(striped_key)
      
      ##Code
      os.mkdir(base_path)
      s3_client.download_file(bucket, key, download_path)
      try:
         Image.open(download_path).verify()
      except BaseException as e:
        #Print to log in cloudwatch
        print(repr(e))
        return 
      resize_image(download_path, thumbnail_path,size)
      s3_client.upload_file(thumbnail_path, bucket, '{}/{}'.format(thumbnail_prefix,striped_key))
      extract_exif(download_path, exif_path)
      s3_client.upload_file(exif_path, bucket, '{}/{}.json'.format(meta_prefix,striped_key))
